FROM ubuntu:16.04

ARG helmver

RUN apt-get update \
  && apt-get install -y apt-transport-https jq nano ssh curl tar dnsutils gnupg lsb-release vim \
  && curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg -o cloudgoogle.gpg && apt-key add cloudgoogle.gpg \
  && echo "deb http://apt.kubernetes.io/ kubernetes-$(lsb_release -cs) main" | tee /etc/apt/sources.list.d/kubernetes.list \
  && apt-get update && apt-get install -y kubectl

RUN curl https://get.helm.sh/helm-v$helmver-linux-amd64.tar.gz -o helm-v$helmver-linux-amd64.tar.gz \
  && tar -zxvf helm-v$helmver-linux-amd64.tar.gz \
  && chmod +x linux-amd64/helm && cp linux-amd64/helm /usr/local/bin/

RUN echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $(lsb_release -cs) main" \
  | tee /etc/apt/sources.list.d/azure-cli.list \
  && curl -L https://packages.microsoft.com/keys/microsoft.asc -o microsoft.asc && apt-key add microsoft.asc \
  && apt-get update && apt-get install azure-cli

RUN apt-get update \
  && apt-get install -y python python-pip \
  && pip install awscli

RUN adduser --quiet --gecos "Cloud Client,IT,0,0" --disabled-password --home /home/cc cc && chown -Rf cc:cc /home/cc

WORKDIR /home/cc/

USER cc:cc
